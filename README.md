# Shifumi Game
Deliverable for a poweshell course during a system administrator bachelor.

## Description
This script lets you play a shifumi game against the computer or another player

## Installation
No need to install, just run the shifumi.ps1 file on a PowerShell terminal

## Idea to improve this script
- Better error management
- Fix output enconding to utf8
- Implement OOP
- Better variables protection
- Implement GUI
- Use a real programming language ;-)
