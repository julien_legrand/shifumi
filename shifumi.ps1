<#  
.SYNOPSIS  
    Shifumi game
.DESCRIPTION  
    This script lets you play a shifumi game against the computer or another player
.NOTES  
    File Name  : shifumi.ps1
    Author     : Julien Legrand LP ASUR 2021-22
    Requires   : PowerShell
Idea to improve this script :
    Better error management
    Fix output enconding to utf8
    Implement OOP
    Beter variables protection
    Implement GUI
    Use a real programming language ;-)
#>
########################
##      Functions      ##
########################
# Display welcome message at the start of the game
function Welcome {
    Clear-Host
    Write-Host "Bienvenu sur Power Shifumi !"
    Write-Host "Le celebre jeu Pierre - Feuille - Ciseau"
    Write-Host "C'est tres simple : la Pierre casse les Ciseaux, les Ciseaux coupent la feuille et la feuille etouffe la Pierre."
}
# Ask user and test for a valid number of player
function Set-PlayerNumber {
    $NumberP = Read-Host "Entrez le nombre de joureur (1 ou 2) " 
    if ($NumberP -eq 1 -or $NumberP -eq 2) {
        return $NumberP
    }
    else {
        Write-Host "Seul les nombres 1 ou 2 sont acceptes"
        Set-PlayerNumber
    }
}
# Ask user and test for a valid player name
function Set-PlayerName {
    $NamePlayer = Read-Host "Veuillez saisir un nom de joueur"
    if ($NamePlayer -ne '' -and $NamePlayer.Length -lt 20) {
        return $NamePlayer
    }
    else {
        Write-Host "Le nom de joueur doit contenir entre 1 et 20 caracteres"
        Set-PlayerName
    }
}
# Ask user and test for a valid number of rounds
function Set-NumberOfRounds {
    $NumberR = Read-Host "Entrez un nombre de partie gagnante"
    if (($NumberR -gt 0) -and ($NumberR -lt 9)) {
        return $NumberR
    }
    else {
        Write-Host "Veuillez saisir un nombre entre 0 et 9"
        Set-NumberOfRounds
    }
}

# Take a hit from player(s), return Pierre, Feuille or Ciseaux
function justDohit {
    param($name)
    $hit = 0
    $hit = Read-Host "$name c'est a toi de jouer, tape 1, 2 ou 3"
    if ($hit -eq 1 -or $hit -eq 2 -or $hit -eq 3) {
        Clear-Host
        switch ($hit) {
            1 { 
                return 'Pierre'
            }
            2 {
                return 'Feuille'
            }
            3 { 
                return 'Ciseaux' 
            }
        }
    }
    else {
        Write-Host 'Seuls les nombres 1, 2 ou 3 sont acceptés' 
        justDohit($name)
    }
}
# Play a round, choose Pierre/Ciseau/Feuille etc, return the winner name or equality
# If only one parameter then this is a round agains the computer
function playRound {
    param (
        $NumberOfPlayer,
        $Player1Name,
        $Player2Name 
    )
    Write-Host "1 = Pierre   2 = Feuille   3 = Ciseaux "
    $Player1hit = justDohit $Player1Name
    # If 2 players
    if ($NumberOfPlayer -eq 2) {
        Write-Host 'Au suivant'
        $Player2hit = justDohit $Player2Name
    }
    # If 1 player
    else {
        $Player2hit = 'Pierre', 'Feuille', 'Ciseaux' | Get-Random
    }
    Write-Host "$Player1Name joue $Player1Hit"
    Write-Host "$Player2Name joue $Player2Hit"
    Write-Host
    if ($Player1hit -eq $Player2hit){
        Write-Host "Egualite ! On recommence !"
        return 'equality'
    }
    # Find and return the winner
    else {
        switch ($Player1hit) {
            'Pierre' {
                if ($Player2hit -eq 'Ciseaux') {
                    Write-Host "La Pierre casse les Ciseaux"
                    return $Player1Name
                }
                else {
                    Write-Host "La Feuille etouffe la Pierre"
                    return $Player2Name
                }
            }
            'Feuille' {
                if ($Player2hit -eq 'Pierre') {
                    Write-Host "La Feuille etouffe la Pierre"
                    return $Player1Name
                }
                else {
                    Write-Host "Les Ciseaux coupent la Feuille"
                    return $Player2Name
                }
            }
            'Ciseaux' {
                if ($Player2hit -eq 'Feuille') {
                    Write-Host "Les Ciseaux coupent la Feuille"
                    return $Player1Name
                }
                else {
                    Write-Host "La Pierre casse les Ciseaux"
                    return $Player2Name
                }
            }
        }
    }
}
# Play a party
function playParty {
    param (
        [Parameter(Mandatory)]
        $NumberOfPlayer,
        $NumberOfRounds,
        $Player1Name,
        $Player2Name
    )
    Clear-Host
    if ($NumberOfPlayer -eq 2){
        write-host "$Player1Name et $Player2Name vous jouez en $NumberOfRounds manche(s)."
    }
    else {
        write-host "$Player1Name vous jouez contre l'ordinateur en $NumberOfRounds manche(s)."
    }
    $Player1Score = 0
    $Player2Score = 0
    while ($Player1Score -lt $NumberOfRounds -and $Player2Score -lt $NumberOfRounds)  {
        $RoundWinner = playRound -NumberOfPlayer $NumberOfPlayer -Player1Name $Player1Name -Player2Name $Player2Name 
        if ($RoundWinner -ne 'equality'){
            Write-Host "1 point pour $RoundWinner"
        }
        switch ($RoundWinner) {
            $Player1Name { $Player1Score += 1 }
            $Player2Name { $Player2Score += 1 }
            Default {}
        }
        Write-Host
        Write-Host "Score :"
        Write-Host $Player1Name $Player1Score
        Write-Host $Player2Name $Player2Score
        Write-Host
    }
    if ($Player1Score -gt $Player2Score) {
        Write-Host "    $Player1Name a gagne !" 
    }
    else {
        Write-Host "    $Player2Name a gagne !" 
    }
    Write-Host
    Write-Host "Que voulez vous faire maintenant ?"
    Write-Host
    Write-Host "    1 Rejouer"
    Write-Host "    2 Revenir au menu pricipal"
    Write-Host "    N'importe quelle touche pour quitter le jeu"
    Write-Host
    $again = Read-Host "Tapez 1,2 ou 3"
    switch ($again) {
        1 { playParty -NumberOfPlayer $NumberOfPlayer -NumberOfRounds $NumberOfRounds -Player1Name $Player1Name -Player2Name $Player2Name}
        2 { Main }
        3 { exit } 
        Default {exit}
    }
    exit
}
########################
##       Main         ##
########################
function Main {
    # Print welcome message and game rules
    Welcome
    # Get players and party information
    $NumberOfPlayer =  Set-PlayerNumber
    Clear-Host
    if ($NumberOfPlayer -eq 1) {
        $Player1Name = Set-PlayerName 
        $Player2Name = "l'Ordinateur" 
        Write-Host "Bienvenu $Player1Name"
    }
    else {
        Write-Host "Joueur 1 "
        $Player1Name = Set-PlayerName
        Clear-Host
        Write-Host "Joueur 2 "
        $Player2Name = Set-PlayerName
        Clear-Host
        Write-Host "Bienvenu $Player1Name"
        Write-Host "Bienvenu $Player2Name"
    }
    $NumberOfRounds = Set-NumberOfRounds
    # Start playing !
    playParty -NumberOfPlayer $NumberOfPlayer -NumberOfRounds $NumberOfRounds -Player1Name $Player1Name -Player2Name $Player2Name
}
########################
##       Script       ##
########################
Main
Exit
